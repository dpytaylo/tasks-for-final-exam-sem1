package task1;

import java.util.ArrayList;

public class StudentStorage {
    private ArrayList<Student> inner;

    public StudentStorage() {
        inner = new ArrayList<Student>();
    }

    public void addStudent(Student student) {
        inner.add(student);
    }

    public void printAllStudentsAfterFollowingYear(int year) {
        System.out.printf("All students after %d year:\n", year);
        for (var student : inner) {
            if (student.getYear() > year) {
                System.out.println(student);
            }
        }
    }
}
