package task3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class AbiturientStorage {
    private ArrayList<Abiturient> inner;

    public AbiturientStorage() {
        inner = new ArrayList<Abiturient>();
    }

    public void addAbiturient(Abiturient abiturient) {
        inner.add(abiturient);
    }

    public void printInAlphabetOrder() {
        System.out.println("All abiturients in alphabet order:");
        inner.sort(Comparator.comparing(Abiturient::getLastName));
        System.out.println(inner);
    }
}
